### Build:
`$ docker-compose build`

### Install Dependencies:
`$ docker-compose run install`

### Run Code Quality Checks:
`$ docker-compose run check code-quality`

### Run Tests
`$ docker-compose run check tests`

### Run Coverage
`$ docker-compose run check coverage`

### Run Fibonacci
`$ docker-compose run math fibonacci {{number}}`


