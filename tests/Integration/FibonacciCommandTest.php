<?php

namespace Tests\Integration;

use Flixbus\Commands\FibonacciCommand;
use Flixbus\Fibonacci\Fibonacci;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class FibonacciCommandTest extends TestCase
{
    public Application $app;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app = new Application();
    }

    /** @test */
    public function it_should_throw_exception_when_required_argument_is_missing(): void
    {
        // Set
        $fibonacci = new Fibonacci();
        $this->app->add(new FibonacciCommand($fibonacci));
        $command = $this->app->find('fibonacci');

        // Expectations
        $this->expectException(RuntimeException::class);

        // Action
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
        ]);
    }

    /** @test */
    public function it_should_get_number_correctly(): void
    {
        // Set
        $fibonacci = new Fibonacci();
        $this->app->add(new FibonacciCommand($fibonacci));
        $command = $this->app->find('fibonacci');

        // Action
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'number'   => 13,
        ]);

        // Assertion
        $output = $commandTester->getDisplay();
        $this->assertSame('233', $output);
    }
}
