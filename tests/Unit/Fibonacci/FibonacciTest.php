<?php
namespace Tests\Unit\Fibonacci;

use Flixbus\Fibonacci\Fibonacci;
use PHPUnit\Framework\TestCase;

class FibonacciTest extends TestCase
{
    /**
     * @test
     * @dataProvider fibonacciCasesDataProvider
     *
     * @param int $input
     * @param int $expectedResult
     */
    public function it_should_calculate(int $input, int $expectedResult): void
    {
        // Set
        $fibonacci =  new Fibonacci();

        // Action
        $result = $fibonacci->getNumber($input);

        // Result
        $this->assertSame($expectedResult, $result);
    }

    public function fibonacciCasesDataProvider(): array
    {
        return [
            'input 0 should return 0' => [
                    'input' => 0,
                    'result' => 0,
            ],
            'input 1 should return 1' => [
                'input' => 1,
                'result' => 1,
            ],
            'input 2 should return 1' => [
                'input' => 2,
                'result' => 1,
            ],
            'input 3 should return 1' => [
                'input' => 3,
                'result' => 2,
            ],
            'input 4 should return 3' => [
                'input' => 4,
                'result' => 3,
            ],
            'input 5 should return 5' => [
                'input' => 5,
                'result' => 5,
            ],
            'input 6 should return 8' => [
                'input' => 6,
                'result' => 8,
            ],
            'input 7 should return 13' => [
                'input' => 7,
                'result' => 13,
            ],
            'input 8 should return 21' => [
                'input' => 8,
                'result' => 21,
            ],
            'input 9 should return 34' => [
                'input' => 9,
                'result' => 34,
            ],
            'input 10 should return 55' => [
                'input' => 10,
                'result' => 55,
            ],
            'input 11 should return 89' => [
                'input' => 11,
                'result' => 89,
            ],
            'input 12 should return 144' => [
                'input' => 12,
                'result' => 144,
            ],
            'input 13 should return 233' => [
                'input' => 13,
                'result' => 233,
            ],
            'input 14 should return 377' => [
                'input' => 14,
                'result' => 377,
            ],
        ];
    }
}
