<?php

namespace Flixbus\Commands;

use Flixbus\Fibonacci\FibonacciInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FibonacciCommand extends Command
{
    protected static $defaultName = 'fibonacci';

    protected FibonacciInterface $fibonacci;

    public function __construct(FibonacciInterface $fibonacci)
    {
        $this->fibonacci = $fibonacci;

        parent::__construct(null);
    }

    protected function configure(): void
    {
        $this->setDescription('Calculates Fibonacci Number');

        $this->addArgument('number', InputArgument::REQUIRED, 'number');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $number = (int) $input->getArgument('number');

        $result = (string) $this->fibonacci->getNumber($number);

        $output->write($result);

        return 0;
    }
}
