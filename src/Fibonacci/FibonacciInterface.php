<?php

namespace Flixbus\Fibonacci;

interface FibonacciInterface
{
    public function getNumber(int $number): int;
}
