<?php

namespace Flixbus\Fibonacci;

class Fibonacci implements FibonacciInterface
{
    public function getNumber(int $number): int
    {
        if ($number === 0) {
            return 0;
        }

        if ($number == 1 || $number == 2) {
            return 1;
        }

        return $this->getNumber($number - 1) + $this->getNumber($number - 2 );
    }
}
